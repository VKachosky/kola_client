var Server_Address = "http://localhost:8000";
var API_Domain_Name = Server_Address+"/api/";

export default {
  SERVER_ADDRESS: Server_Address,
  REGISTER_USER: API_Domain_Name + "auth/register",
  LOGIN_USER: API_Domain_Name + "auth/login",
  INFORMATION_USER: API_Domain_Name + "user",
  LIST_PROFESSION: API_Domain_Name + "professions",
  HOME: API_Domain_Name + "home",
  LIST_TEAM : API_Domain_Name + "teams",
  LIST_TASK: API_Domain_Name + "tasks",
  CREATE_TEAM: API_Domain_Name + "team/create",
  LIST_USERS: API_Domain_Name + "list_users",
  ADD_MEMBER: API_Domain_Name + "team/add_member",
  LIST_MEMBER: API_Domain_Name + "team",
  CREATE_TASK: API_Domain_Name + "task/create",
  ADD_WORKER: API_Domain_Name + "task/add_worker"
};
