import React, { Component } from 'react';
import { Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import axios from "axios/index";
import ApiRoutes from '../../../constants/ApiRoutes'
import { Redirect} from 'react-router-dom';

class Register extends Component {
  state = {
    selectedOption: '',
    roleSelected: '',
    name: '',
    password: '',
    email: '',
    confirmPassword: '',
    professions_options : [],
    roles_options: [],
    authenticated: false
  };

  componentWillMount(){
    var url = ApiRoutes.LIST_PROFESSION
    axios({
      method: 'get',
      url: url,
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          let professions = response.data.data.professions.map(profession =>
            (<option key={profession.id} value={profession.id} label={profession.name}> {profession.name}</option>)
          )

          let roles = response.data.data.roles.map(role =>
            (<option key={role.id} value={role.id} label={role.name}> {role.name}</option>)
          )
          this.setState({professions_options: professions, roles_options: roles})
        }
        else if(response.data.status === 1){
          alert(response.data.message)
        }
        else {
          alert(response.data.error)
        }
      })
      .catch(error => {
        alert(error.message)
      })
  }

  onEmailChange = (event) => {
    this.setState({email: event.target.value})
  }

  onPasswordChange = (event) => {
    this.setState({password: event.target.value})
  }

  onNameChange = (event) => {
    this.setState({name: event.target.value})
  }

  onConfirmPasswordChange = (event)  => {
    this.setState({confirmPassword: event.target.value})
  }

  handleSubmit = (event)=> {
    if(this.state.password === this.state.confirmPassword){
      var data = {
        'email': this.state.email,
        'password': this.state.password,
        'name': this.state.name,
        'profession_id': this.state.selectedOption,
        'user_type': this.state.roleSelected
      };
      var url = ApiRoutes.REGISTER_USER
      axios({
        method: 'post',
        url: url,
        data: data,
        config: {headers: {'Content-Type': 'application/json'}}
      })
        .then(response => {
          if(response.data.status === 1 && response.data.data){
            var user = {
              "token": response.data.data.token,
              "id": response.data.data.id,
              "type": response.data.data.user_type
            }
            localStorage.setItem('user', JSON.stringify(user))
            this.props.history.push('/tasks')
          }
          else if(response.data.status === 1){
            alert(response.data.message)
          }
          else {
            alert(response.data.error)
          }
        })
    }
    event.preventDefault()
  }

  renderSelectProfessions (){
    return(
      <Input type="select"
             value={this.state.selectedOption}
             onChange={event => this.setState({selectedOption: event.target.value})}>
        <option>Select your profession</option>
        {this.state.professions_options}
      </Input>
    )
  }

  renderSelectRoles (){
    return(
      <Input type="select"
             value={this.state.roleSelected}
             onChange={event => this.setState({roleSelected: event.target.value})}>
        <option>Select your role</option>
        {this.state.roles_options}
      </Input>
    )
  }

  redirect(){
    if (this.state.authenticated) return (<Redirect to="/dashboard"/>)
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          {
            this.redirect()
          }
          <Row className="justify-content-center">
            <Col md="9" lg="7" xl="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form onSubmit={this.handleSubmit}>
                    <h1 className="text-center">Register</h1>
                    <p className="text-muted; text-center">Create your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-info"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      {this.renderSelectRoles()}
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-briefcase"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      {this.renderSelectProfessions()}
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="Full name" autoComplete="name"
                             value={this.state.name}
                             onChange={this.onNameChange}
                      />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>@</InputGroupText>
                      </InputGroupAddon>
                      <Input type="email" placeholder="Email" autoComplete="email"
                             value={this.state.email}
                             onChange={this.onEmailChange}
                      />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" placeholder="Password" autoComplete="new-password"
                             value={this.state.password}
                             onChange={this.onPasswordChange}
                      />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" placeholder="Repeat password" autoComplete="new-password"
                             value={this.state.confirmPassword}
                             onChange={this.onConfirmPasswordChange}
                      />
                    </InputGroup>
                    <Button color="success" block>Create Account</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Register;
