import React, { Component } from 'react';
import { Link, Redirect, Route, Switch} from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Toast, ToastBody, ToastHeader } from 'reactstrap';
import axios from 'axios';
import ApiRoutes from '../../../constants/ApiRoutes'

class Login extends Component {
  constructor(props){
    super(props)
    this.state = {
      email : '',
      password: '',
      authenticated: false
    }

    this.onEmailChange = this.onEmailChange.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this)
  }


  onEmailChange(event){
    this.setState({email: event.target.value})
  }

  onPasswordChange(event){
    this.setState({password: event.target.value})
  }

  handleSubmit(event){
    var data = {
      'email': this.state.email,
      'password': this.state.password
    };
    var url = ApiRoutes.LOGIN_USER;
    axios({
      method: 'post',
      url: url,
      data: data,
      config: {headers: {'Content-Type': 'application/json'}}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          var user = {
            "token": response.data.data.token,
            "id": response.data.data.id,
            "type": response.data.data.user_type
          }
          localStorage.setItem('user', JSON.stringify(user))
          this.setState({authenticated: true})
          /*return(
            <div>
              <div className="p-3 bg-success my-2 rounded">
                <Toast>
                  <ToastHeader>
                    Information
                  </ToastHeader>
                  <ToastBody>
                    Login Successful, {response.data.data.message}
                  </ToastBody>
                </Toast>
              </div>
            </div>
            /*<Switch>
            <Route path="/dashboard" name="Dashboard" render={props => <Dashboard {...props}/>} />
          </Switch>)*/
        }
        else if(response.data.status === 1){
          alert(response.data.message)
        }
        else {
          alert(response.data.error)
        }
      })
      .catch(error => {
        alert(error.message)
      })
    event.preventDefault()
  }

  redirect(){
    var user = JSON.parse(localStorage.getItem('user'))
    if (this.state.authenticated) return this.props.history.push('/tasks')
  }


  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          {
            this.redirect()
          }
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={this.handleSubmit}>
                      <h1 className="text-center">Login</h1>
                      <p className="text-muted, text-center">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            @
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="email" placeholder="email" autoComplete="email"
                               value={this.state.email}
                               onChange={this.onEmailChange}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" autoComplete="current-password"
                               value={this.state.password}
                               onChange={this.onPasswordChange}
                        />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">Login</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0" disabled={true}>Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>if you do not have an account in the application, please register.</p>
                      <Link to="/register">
                        <Button color="primary" className="mt-3" active tabIndex={-1}>Register Now!</Button>
                      </Link>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
