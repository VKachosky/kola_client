import React, { Component } from 'react';
import {Card, CardBody, CardHeader, Col, Row, Table, Button, Modal, ModalBody, ModalFooter, ModalHeader, FormGroup, Form, Label, Input } from 'reactstrap';
import ApiRoutes from  '../../constants/ApiRoutes'
import axios from "axios/index";

class TaskComponent extends Component {

  constructor(props) {
    super(props);

    this.state = {
      task_tr: [],
      addTaskModalVisible: false,
      taskName: '',
      taskPriority: '',
      taskEndDate: '',
      taskDescription: '',
      teamSelect: '',
      teams_options: [],
      addWorkerModalVisible: false,
      users_options: [],
      userSelect: ''
    };

    this.task_id = 0
    this.team_id = 0
  }

  onTaskNameChange = (event) => {
    this.setState({taskName: event.target.value})
  }

  onTaskEndDateChange = (event) => {
    this.setState({taskEndDate: event.target.value})
  }

  onTaskPriorityChange = (event) => {
    this.setState({taskPriority: event.target.value})
  }

  onTaskDescriptionChange = (event) => {
    this.setState({taskDescription: event.target.value})
  }


  componentWillMount(){
    var {id, token, type} = JSON.parse(localStorage.getItem('user'))
    var url = ApiRoutes.LIST_TASK;
    axios({
      method: 'post',
      url: url,
      data: {
        user_id: id
      },
      headers: {token: token}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          let tasks = response.data.data.member_task.map(item => (
            <tr key={item.Task.id}>
              <td>{item.Task.name}</td>
              <td>{(item.User)?item.User.name:''}</td>
              {(item.Task.priority === 'high')?
                <td className="bg-danger text-center">{item.Task.priority}</td>:
                (item.Task.priority === 'medium')?<td className="bg-warning text-center">{item.Task.priority}</td>:
                  <td className="bg-success text-center">{item.Task.priority}</td>
              }
              {(item.Task.status === 'locked')?
                <td className="bg-danger text-center">{item.Task.status}</td>:
                (item.Task.status === 'pending')?<td className="bg-primary text-center">{item.Task.status}</td>:
                  (item.Task.status === 'ongoing')?<td className="bg-warning text-center">{item.Task.status}</td>:
                  <td className="bg-success text-center">{item.Task.status}</td>
              }
              <td className="text-center">{item.Task.endDate}</td>
              <td className="text-center">
                {(! item.User && item.type === 'admin' )?
                  <i title="add worker" className="fa fa-user-plus" onClick={() => this.onAddWorkerButtonClick(item.Task.id)}/>: ''}
              </td>
            </tr>
          ))

          this.setState({task_tr: tasks})
        }
        else if(response.data.status === 1){
          alert(response.data.message)
        }
        else {
          alert(response.data.error)
        }
      })
      .catch(error => {
        alert(error.message)
      })
  }

  onTeamSelectChange = (event) => {
    this.setState({teamSelect: event.target.value})
  }


  onUserSelectChange = (event) => {
    this.setState({userSelect: event.target.value})
  }

  renderTaksTr(){
    if(this.state.task_tr.length !== 0){
      return this.state.task_tr
    }
    else return <tr></tr>
  }

  renderTeamsOptions(){
    if(this.state.teams_options.length !== 0){
      return this.state.teams_options
    }
    else return []
  }

  renderUsersOptions(){
    if(this.state.users_options.length !== 0){
      return this.state.users_options
    }
    else return []
  }

  toggleAddTask = () => {
    this.setState(prevState => ({
      addTaskModalVisible: !prevState.addTaskModalVisible
    }));
  }

  toggleAddWorker = () => {
    this.setState(prevState => ({
      addWorkerModalVisible: !prevState.addWorkerModalVisible
    }))
  }

  onCreateTaskSubmit = () => {
    localStorage.setItem('taskTeam_id', this.state.teamSelect)
    var {token, id} = JSON.parse(localStorage.getItem('user'))
    var task = {
      'name': this.state.taskName,
      'endDate': this.state.taskEndDate,
      'priority': this.state.taskPriority,
      'description': this.state.taskDescription
    }
    var url = ApiRoutes.CREATE_TASK
    axios({
      method: 'post',
      url: url,
      data: {
        user_id: id,
        task: task,
        team_id: this.state.teamSelect
      },
      headers: {token: token}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          this.setState(prevState => ({
            addTaskModalVisible: !prevState.addTaskModalVisible
          }))
        }
        else if(response.data.status === 1){
          alert(response.data.message)
          this.setState(prevState => ({
            addTaskModalVisible: !prevState.addTaskModalVisible
          }))
        }
        else {
          alert(response.data.error)
        }
      })
      .catch(error => {
        alert(error.message)
      })
  }

  onCreateTaskButtonClick = () => {
    var {token, id} = JSON.parse(localStorage.getItem('user'))
    var url = ApiRoutes.LIST_TEAM
    axios({
      method: 'post',
      url: url,
      data: {
        user_id: id
      },
      headers: {token: token}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          let teams_options = response.data.data.teams.map(item => (
            <option key={item.team_id} value={item.team_id}>{item.Team.name}</option>
          ))

          this.setState({teams_options: teams_options, addTaskModalVisible: true})
        }
        else if(response.data.status === 1){
          alert(response.data.message)
        }
        else {
          alert(response.data.error)
        }
      })
  }

  onAddWorkerButtonSubmit = () => {
    var {token, id} = JSON.parse(localStorage.getItem('user'))
    var url = ApiRoutes.ADD_WORKER
    axios({
      method: 'post',
      url: url,
      data: {
        user_id: this.state.userSelect,
        team_id: localStorage.getItem('taskTeam_id'),
        task_id: this.task_id
      },
      headers: {token: token}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          this.setState(prevState => ({
            addWorkerModalVisible: !prevState.addWorkerModalVisible
          }))
        }
        else if(response.data.status === 1){
          alert(response.data.message)
          this.setState(prevState => ({
            addWorkerModalVisible: !prevState.addWorkerModalVisible
          }))
        }
        else {
          alert(response.data.error)
        }
      })
  }

  onAddWorkerButtonClick = (task_id) => {
    this.task_id = task_id
    var {token, id} = JSON.parse(localStorage.getItem('user'))
    var url = ApiRoutes.LIST_MEMBER
    axios({
      method: 'post',
      url: url,
      data: {
        user_id: id,
        team_id: localStorage.getItem('taskTeam_id')
      },
      headers: {token: token}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          let teams_options = response.data.data.team_users.map(item => (
            <option key={item.User.id} value={item.User.id}>{item.User.email}</option>
          ))

          this.setState({users_options: teams_options, addWorkerModalVisible: true})
        }
        else if(response.data.status === 1){
          alert(response.data.message)
        }
        else {
          alert(response.data.error)
        }
      })
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Modal isOpen={this.state.addTaskModalVisible} toggle={this.toggleAddTask}>
          <ModalHeader toggle={this.toggleAddTask}>Create task</ModalHeader>
          <ModalBody>
            <Form onSubmit= {this.handleSubmit}>
              <FormGroup>
                <Label for="teamTask">Team</Label>
                <Input type="select"
                       value={this.state.teamSelect}
                       onChange={this.onTeamSelectChange}>
                  <option>select the team allocated to the task</option>
                  {this.renderTeamsOptions()}
                </Input>
              </FormGroup>
              <FormGroup>
                <Label for="taskName">Name</Label>
                <Input type="text" placeholder="Enter the name"
                       value={this.state.taskName}
                       onChange={this.onTaskNameChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="endDate">End date</Label>
                <Input type="date" placeholder="Enter the end date "
                       value={this.state.taskEndDate}
                       onChange={this.onTaskEndDateChange}
                />
              </FormGroup>
              <FormGroup>
                <Label for="priorityTask">Priority</Label>
                <Input type="select"
                       value={this.state.taskPriority}
                       onChange={this.onTaskPriorityChange}>
                  <option value="">Select the priority</option>
                  <option value="high">High</option>
                  <option value="medium">Medium</option>
                  <option value="low">Low</option>
                </Input>
              </FormGroup>
            </Form>
            <FormGroup>
              <Label for="descriptionTask">Description</Label>
              <Input type="textarea" placeholder="Enter the description"
                     value={this.state.taskDescription}
                     onChange={this.onTaskDescriptionChange}
              />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button color="success" onClick={this.onCreateTaskSubmit}>Add</Button>
            <Button color="danger" onClick={this.toggleAddTask}>Cancel</Button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={this.state.addWorkerModalVisible} toggle={this.toggleAddWorker}>
          <ModalHeader toggle={this.toggleAddWorker}>Add worker</ModalHeader>
          <ModalBody>
            <Form onSubmit= {this.onAddWorkerButtonSubmit}>
              <FormGroup>
                <Label for="teamTask">User</Label>
                <Input type="select"
                       value={this.state.userSelect}
                       onChange={this.onUserSelectChange}>
                  <option>select the user allocated to the task</option>
                  {this.renderUsersOptions()}
                </Input>
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="success" onClick={this.onAddWorkerButtonSubmit}>Add</Button>
            <Button color="danger" onClick={this.toggleAddWorker}>Cancel</Button>
          </ModalFooter>
        </Modal>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Tasks of the week
                <Button  className="pull-right" color="success" onClick={this.onCreateTaskButtonClick}><i className="fa fa-plus-square"> Add task</i></Button>
              </CardHeader>
              <CardBody>
                <Table responsive hover striped >
                  <thead className="thead-light">
                  <tr>
                    <th className="text-justify">Name</th>
                    <th className="text-justify">Responsable</th>
                    <th className="text-center">Priority</th>
                    <th className="text-center">Status</th>
                    <th className="text-center">End date</th>
                    <th className="text-center">Actions</th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.renderTaksTr()
                  }
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    );
  }
}

export default TaskComponent;
