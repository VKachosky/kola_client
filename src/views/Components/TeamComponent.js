import React, { Component } from 'react';
import {Card, CardBody, CardHeader, Col, Row, Table, Button, ModalHeader, ModalFooter, ModalBody, Modal, Form, FormGroup, Input, Label } from 'reactstrap';
import ApiRoutes from  '../../constants/ApiRoutes'
import axios from "axios/index";
import {Redirect} from 'react-router-dom';

class TeamComponent extends Component {

  constructor(props) {
    super(props);

    this.team_id = 0

    this.state = {
      userSelect: '',
      team_tr: [],
      addTeamModalVisible: false,
      nameTeam: '',
      roleTeam: '',
      contextTeam: '',
      descriptionTeam: '',
      teamCreated: false,
      addMemberModalVisible: false,
      users: [],
      isAdminMember: false

    };
  }

  toggleAddTeam = () => {
    this.setState(prevState => ({
      addTeamModalVisible: !prevState.addTeamModalVisible
    }));
  }

  toggleAddMemberTeam = () => {
    this.setState(prevState => ({
      addMemberModalVisible: !prevState.addMemberModalVisible
    }));
  }

  onNameTeamChange = (event) => {
    this.setState({nameTeam: event.target.value})
  }

  onRoleTeamChange = (event) => {
    this.setState({roleTeam: event.target.value})
  }

  onContextTeamChange = (event) => {
    this.setState({contextTeam: event.target.value})
  }

  onDescriptionTeamChange = (event) => {
    this.setState({descriptionTeam: event.target.value})
  }

  handleSubmit = () => {
    var {token, id} = JSON.parse(localStorage.getItem('user'))
    var team = {
      'name': this.state.nameTeam,
      'role': this.state.roleTeam,
      'context': this.state.contextTeam,
      'description': this.state.descriptionTeam
    }
    var url = ApiRoutes.CREATE_TEAM
    axios({
      method: 'post',
      url: url,
      data: {
        user_id: id,
        team: team
      },
      headers: {token: token}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          this.setState(prevState => ({
            addTeamModalVisible: !prevState.addTeamModalVisible
          }))
        }
        else if(response.data.status === 1){
          this.setState({teamCreated: true})
        }
        else {
          alert(response.data.error)
        }
      })
  }

  addTeamMember = () => {
    alert(this.state.isAdminMember)
    var {token} = JSON.parse(localStorage.getItem('user'))
    var url = ApiRoutes.ADD_MEMBER
    axios({
      method: 'post',
      url: url,
      data: {
        user_id: this.state.userSelect,
        team_id: this.team_id,
        isAdmin: this.state.isAdminMember
      },
      headers: {token: token}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){

        }
        else if(response.data.status === 1){
          alert(response.data.message)
          if(this.state.isAdminMember) this.setState({isAdminMember: false})
          this.setState(prevState => ({
            addMemberModalVisible: !prevState.addMemberModalVisible
          }))
        }
        else {
          alert(response.data.error)
        }
      })
  }

  onAddMemberClick = (team_id) => {
    this.team_id = team_id
    var {token, id} = JSON.parse(localStorage.getItem('user'))
    var url = ApiRoutes.LIST_USERS
    axios({
      method: 'post',
      url: url,
      data: {
        user_id: id
      },
      headers: {token: token}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          let users_options = response.data.data.users.map(user => (
            <option label={user.email} value={user.id} key={user.id}>{user.email}</option>
          ))
          this.setState({users: users_options, addMemberModalVisible: true})
        }
        else if(response.data.status === 1){
          alert(response.data.message)
        }
        else {
          alert(response.data.error)
        }
      })
  }

  onMembersClick = (team_id) => {
    localStorage.setItem('team_id', team_id)
    this.props.history.push('/members')
  }


  componentWillMount(){
    var {id, token} = JSON.parse(localStorage.getItem('user'))
    var url = ApiRoutes.LIST_TEAM;
    axios({
      method: 'post',
      url: url,
      data: {
        user_id: id
      },
      headers: {token: token}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          let teams = response.data.data.teams.map(item => (
            <tr key={item.Team.id}>
              <td>{item.Team.name}</td>
              <td >{item.Team.role}</td>
              <td>{item.Team.context}</td>
              <td>{item.Team.description}</td>
              <td className="text-center">
                {(item.type === 'admin')?<i title="add member" color="success" className="fa fa-user-plus" onClick={() => this.onAddMemberClick(item.Team.id)}/>: ''}
                <i title="list members" className="fa fa-users" onClick={() => this.onMembersClick(item.Team.id)}/>
              </td>
            </tr>
          ))

          this.setState({team_tr: teams})
        }
        else if(response.data.status === 1){
          alert(response.data.message)
        }
        else {
          alert(response.data.error)
        }
      })
      .catch(error => {
        alert(error.message)
      })
  }

  redirect(){
    if(this.state.teamCreated) return (<Redirect to="/members"/>)
  }

  renderTeamsTr(){
    if(this.state.team_tr.length !== 0){
      return this.state.team_tr
    }
    else return <tr></tr>
  }

  renderSelectUsers (){
    return(
      <Input type="select"
             value={this.state.userSelect}
             onChange={event => this.setState({userSelect: event.target.value})}>
        <option>Select an email</option>
        {this.state.users}
      </Input>
    )
  }


  render() {
    return (
      <div className="animated fadeIn">
        {this.redirect()}
        <div>
          <Modal isOpen={this.state.addTeamModalVisible} toggle={this.toggleAddTeam}>
            <ModalHeader toggle={this.toggleAddTeam}>Create team</ModalHeader>
            <ModalBody>
              <Form onSubmit= {this.handleSubmit}>
                <FormGroup>
                  <Label for="nameTask">Name</Label>
                  <Input type="text" placeholder="Enter the name "
                         value={this.state.nameTeam}
                         onChange={this.onNameTeamChange}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="roleTask">Role</Label>
                  <Input type="text" placeholder="Enter the role "
                         value={this.state.roleTeam}
                         onChange={this.onRoleTeamChange}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="nameTask">Context</Label>
                  <Input type="text" placeholder="Enter the context"
                         value={this.state.contextTeam}
                         onChange={this.onContextTeamChange}
                  />
                </FormGroup>
              </Form>
              <FormGroup>
                <Label for="nameTask">Description</Label>
                <Input type="textarea" placeholder="Enter the description"
                       value={this.state.descriptionTeam}
                       onChange={this.onDescriptionTeamChange}
                />
              </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button color="success" onClick={this.handleSubmit}>Add</Button>
              <Button color="danger" onClick={this.toggleAddTeam}>Cancel</Button>
            </ModalFooter>
          </Modal>
          <Modal isOpen={this.state.addMemberModalVisible} toggle={this.toggleAddMemberTeam}>
            <ModalHeader toggle={this.toggleAddMemberTeam}>Add member</ModalHeader>
            <ModalBody>
              <Form onSubmit= {this.addTeamMember}>
                <FormGroup>
                  <Label for="email">Email</Label>
                  {this.renderSelectUsers()}
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input type="checkbox"
                           value={this.state.isAdminMember}
                           onChange={(event) => {this.setState({isAdminMember: event.target.checked})}}
                    /> Is admin?
                  </Label>
                </FormGroup>
              </Form>
            </ModalBody>
            <ModalFooter>
              <Button color="success" onClick={this.addTeamMember}>Add</Button>
              <Button color="danger" onClick={this.toggleAddMemberTeam}>Cancel</Button>
            </ModalFooter>
          </Modal>
        </div>
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> All teams
                <Button  className="pull-right" color="success" onClick={() => {this.setState({addTeamModalVisible: true})}}><i className="fa fa-plus-square"> Add team</i></Button>
              </CardHeader>
              <CardBody>
                <Table responsive hover striped >
                  <thead className="thead-light">
                  <tr>
                    <th className="text-justify">Name</th>
                    <th className="text-justify">Role</th>
                    <th className="text-justify">Context</th>
                    <th className="text-justify">Description</th>
                    <th className="text-center">Actions</th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.renderTeamsTr()
                  }
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    );
  }
}

export default TeamComponent;
