import React, { Component } from 'react';
import {Card, CardBody, CardHeader, Col, Row, Table, Button } from 'reactstrap';
import ApiRoutes from  '../../constants/ApiRoutes'
import axios from "axios/index";

class TeamMembersComponent extends Component {

  constructor(props) {
    super(props);

    this.state = {
      members_tr: []
    };
  }

  componentWillMount(){
    var {id, token, type} = JSON.parse(localStorage.getItem('user'))
    var url = ApiRoutes.LIST_MEMBER;
    axios({
      method: 'post',
      url: url,
      data: {
        user_id: id,
        team_id: localStorage.getItem('team_id')
      },
      headers: {token: token}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          let members = response.data.data.team_users.map(item => (
            <tr>
              <td>{item.User.name}</td>
              <td>{item.User.email}</td>
              <td>{(item.type === 'admin')? 'team leader': 'team member'}</td>
            </tr>
          ))

          this.setState({members_tr: members })
        }
        else if(response.data.status === 1){
          alert(response.data.message)
        }
        else {
          alert(response.data.error)
        }
      })
      .catch(error => {
        alert(error.message)
      })
  }

  renderMembersTr(){
    if(this.state.members_tr.length !== 0){
      return this.state.members_tr
    }
    else return <tr></tr>
  }


  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-users"></i> Members of team
              </CardHeader>
              <CardBody>
                <Table responsive hover striped >
                  <thead className="thead-light">
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.renderMembersTr()
                  }
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    );
  }
}

export default TeamMembersComponent;
