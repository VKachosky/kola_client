import React, { Component, lazy, Suspense } from 'react';
import {
  Badge,
  Button,
  ButtonDropdown,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  CardTitle,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Progress,
  Row,
  Table,
} from 'reactstrap';
import axios from "axios/index";
import ApiRoutes from "../../constants/ApiRoutes";



class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      doneTaskNumber: 0,
      ongoingTaskNumber: 0,
      blockedTaskNumber: 0,
      taskNumber: 0,
      member_task_tr: [],
      task_tr: []
    };
  }

  componentWillMount(){
    var {id, token} = JSON.parse(localStorage.getItem('user'))
    var url = ApiRoutes.HOME;
    axios({
      method: 'post',
      url: url,
      data: {
        user_id: id
      },
      headers: {token: token}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          let tasks = response.data.data.member_task.map(item => (
            <tr key={item.Task.id}>
              <td>{item.Task.name}</td>
              <td>{(item.User)?item.User.name:''}</td>
              {(item.Task.priority === 'high')?
                <td className="bg-danger text-center">{item.Task.priority}</td>:
                (item.Task.priority === 'medium')?<td className="bg-warning text-center">{item.Task.priority}</td>:
                  <td className="bg-success text-center">{item.Task.priority}</td>
              }
              {(item.Task.status === 'locked')?
                <td className="bg-danger text-center">{item.Task.status}</td>:
                (item.Task.status === 'pending')?<td className="bg-primary text-center">{item.Task.status}</td>:
                  (item.Task.status === 'ongoing')?<td className="bg-warning text-center">{item.Task.status}</td>:
                  <td className="bg-success text-center">{item.Task.status}</td>
              }
              <td className="text-center">{item.Task.endDate}</td>
              <td className="text-center">
                {(! item.User && item.type === 'admin' )?
                  <i title="add worker" className="fa fa-user-plus" onClick={() => this.onAddWorkerButtonClick(item.Task.id)}/>: ''}
              </td>
            </tr>
          ))

          this.setState({task_tr: tasks, taskNumber: response.data.data.taskNumber, ongoingTaskNumber: response.data.data.ongoingTaskNumber, doneTaskNumber: response.data.data.doneTaskNumber, blockedTaskNumber: response.data.data.blockedTaskNumber})
        }
        else if(response.data.status === 1){
          alert(response.data.message)
        }
        else {
          alert(response.data.error)
        }
      })
      .catch(error => {
        alert(error.message)
      })
  }

  renderTaksTr(){
    if(this.state.task_tr.length !== 0){
      return this.state.task_tr
    }
    else return <tr></tr>
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {

    return (
      <div className="animated fadeIn">
        {this.loading}
        <Row>
          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-info">
              <CardBody className="pb-0">
                <div className="text-value">70% Done taks</div>
              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-primary">
              <CardBody className="pb-0">
                <div className="text-value">{this.state.doneTaskNumber} Done taks</div>
              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-warning">
              <CardBody className="pb-0">
                <div className="text-value">{this.state.ongoingTaskNumber} ongoing tasks</div>
              </CardBody>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-danger">
              <CardBody className="pb-0">
                <div className="text-value">{this.state.blockedTaskNumber} blocked tasks</div>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Tasks of the day
              </CardHeader>
              <CardBody>
                <Table hover responsive  striped className="table-outline mb-0 d-none d-sm-table">
                  <thead className="thead-light">
                  <tr>
                    <th>Name of task</th>
                    <th>Responsable</th>
                    <th>Priority</th>
                    <th>Status</th>
                    <th>End date</th>
                    <th>Description</th>
                  </tr>
                  </thead>
                  <tbody>
                  {
                    this.renderTaksTr()
                  }
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Dashboard;
