import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Badge, UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, Modal, ModalBody, ModalFooter, ModalHeader, Button } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppAsideToggler, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/logo.svg'
import sygnet from '../../assets/img/brand/sygnet.svg'
import ApiRoutes from "../../constants/ApiRoutes";
import axios from "axios/index";

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  state = {
    modal: false,
    user: {}
  }
  toggle = () => {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  onProfilClick = () => {
    var {id, token} = JSON.parse(localStorage.getItem('user'))
    var url = ApiRoutes.INFORMATION_USER;
    axios({
      method: 'post',
      url: url,
      data: {
        user_id: id
      },
      headers: {token : token}
    })
      .then(response => {
        if(response.data.status === 1 && response.data.data){
          this.setState({user: response.data.data.user, modal: true})
        }
        else if(response.data.status === 1){
          alert(response.data.message)
        }
        else {
          alert(response.data.error)
        }
      })
      .catch(error => {
        alert(error.message)
      })
  }

  render(){

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full= 'Monday'
          minimized= "Monday"
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link" ></NavLink>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/users" className="nav-link"></Link>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="#" className="nav-link"></NavLink>
          </NavItem>
        </Nav>
        <div>
            <Modal isOpen={this.state.modal} toggle={this.toggle}>
              <ModalHeader toggle={this.toggle}>Profile</ModalHeader>
              <ModalBody>
                 name : <strong>{this.state.user.name? this.state.user.name: ''}</strong><br/>
                 email : <strong>{this.state.user.email? this.state.user.email: ''}</strong><br/>
                 profession : <strong>{this.state.user.profession?this.state.user.profession: ''}</strong>
              </ModalBody>
              <ModalFooter>
                <Button color="danger" onClick={this.toggle}>Cancel</Button>
              </ModalFooter>
            </Modal>
        </div>
        <Nav className="ml-auto" navbar>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <img src={'../../assets/img/avatars/avatar04.png'} className="img-avatar" alt="#"/>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem>
              <DropdownItem onClick={this.onProfilClick}><i className="fa fa-user"></i> Profile</DropdownItem>
              <DropdownItem divider />
              <DropdownItem onClick={e => {localStorage.removeItem('user'); this.props.onLogout(e)}}><i className="fa fa-lock"></i> Logout</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
